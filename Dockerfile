FROM php:7.3.0-apache

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && apt-get update && apt-get install -y git libzip-dev unzip \
    && docker-php-ext-install zip \
    && a2enmod rewrite headers

RUN docker-php-ext-install pdo pdo_mysql

COPY ./techolution-offers /var/www/html/techolution-offers

WORKDIR /var/www/html/techolution-offers

RUN composer install

RUN apt-get update && apt-get install -y gnupg2

RUN apt-get install -y curl \
    && curl -sL https://deb.nodesource.com/setup_11.x | bash \
    && apt-get install  -y nodejs

RUN npm install -y yarn -g

RUN yarn build